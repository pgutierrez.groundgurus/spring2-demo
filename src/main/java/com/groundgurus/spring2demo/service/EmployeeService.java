package com.groundgurus.spring2demo.service;

import com.groundgurus.spring2demo.model.Employee;
import com.groundgurus.spring2demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployeeService {
    @Autowired
    private EmployeeRepository myEmployeeRepository;

    public List<Employee> getEmployees() {
        return myEmployeeRepository.findAll();
    }

    public void saveEmployee(Employee myEmployee) {
       myEmployeeRepository.save(myEmployee);
    }
}
