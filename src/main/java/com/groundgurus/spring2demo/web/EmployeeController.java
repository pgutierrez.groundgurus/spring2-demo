package com.groundgurus.spring2demo.web;

import com.groundgurus.spring2demo.model.Employee;
import com.groundgurus.spring2demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Create     - POST
 * Retrieve   - GET
 * Update     - PUT
 * Delete     - DELETE
 *
 */
@RestController
@RequestMapping("/api/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService myEmployeeService;

    @GetMapping
    public List<Employee> getEmployees() {
        return myEmployeeService.getEmployees();
    }

    @PostMapping
    public void createEmployee(@RequestBody Employee myEmployee) {
        myEmployeeService.saveEmployee(myEmployee);
    }
}
